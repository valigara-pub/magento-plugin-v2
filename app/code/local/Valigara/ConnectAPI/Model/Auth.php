<?php
$GLOBALS['Valigara_ConnectAPI_Auth_counter'] = 1;
$GLOBALS['Valigara_Connected'] = 0;
$GLOBALS['Valigara_SOAP_Connected'] = 0;
class Valigara_ConnectAPI_Model_Auth extends Mage_Core_Model_Config_Data {
    protected $api_url = 'https://media.valigara.com/client_api.php';
    
    protected function _construct() {
        parent::_construct();
    }

    protected function _beforeSave() {
        switch ($this->getFieldName()) {
            case "valigara_username":
            case "valigara_password":
                $GLOBALS['Valigara_ConnectAPI_Auth_counter']++;
                break;
            case "valigara_connected":
                if($GLOBALS['Valigara_ConnectAPI_Auth_counter'] > 2) {
                    if($this->valigara_auth()) {
                        $this->setValue('Logged in to Valigara API');
                        $GLOBALS['Valigara_ConnectAPI_Auth_counter']++;
                        $GLOBALS['Valigara_Connected'] = 1;
                    } else {
                        $this->setValue('Not logged in');
                        $GLOBALS['Valigara_Connected'] = 0;
                    }
                }
                break;
            case "soap_username":
                break;
            case "soap_apikey":
                break;
            case "soap_apiurl":
                if($GLOBALS['Valigara_Connected']) {
                    if(function_exists('curl_version')) {
                        $this->check_soap_connection();
                    }
                }
                break;
            case "soap_connected":
                if  ($GLOBALS['Valigara_SOAP_Connected']) {
                    $this->setValue('Connected to Magento SOAP/XML API');
                } else {
                    $this->setValue('Not Connected');
                }
                break;
            default:
                break;
        }
    }
    
    protected function _afterSave() {
        if($this->getFieldName() == "valigara_connected") {
            if($this->getValue() == 'Logged in to Valigara API') {
                Mage::getSingleton('core/session')->addSuccess('Logged in Successfully to Valigara API'); 
            } else {
                if(!function_exists('curl_version')) {
                    Mage::getSingleton('core/session')->addError('CURL is not installed on your server. Please install it and try again.');
                } else {
                    Mage::getSingleton('core/session')->addError('Couldn\'t login to Valigara. Please check your username / password.');
                }
            }
        }
    }
    
    protected function getFieldName() {
        return $this->_data["field"];
    }
    
    public function getValueByFieldset($key, $fieldset='') {
        if($fieldset=='') {
            parent::getFieldsetDataValue($key);
        } else {
            if(isset($this->_data['groups'][$fieldset]['fields'][$key])) {
                return $this->_data['groups'][$fieldset]['fields'][$key]["value"];
            }
        }
        return false;
    }
    public function valigara_auth() {
        $data = array(
                "origin" => "magento",
                "username" => $this->getFieldsetDataValue("valigara_username"),
                "password" => $this->getFieldsetDataValue("valigara_password"),
            );
        $result = $this->valigara_curl($data);
//        Mage::getSingleton('core/session')->addError(print_r($result));
        if(@$result["status"] === "100") {
            return true;
        } 
        return false;
    }
    
    private function check_soap_connection() {
        $data = array(
                "origin" => "magento",
                "username" => $this->getValueByFieldset("valigara_username", "authentication"),
                "password" => $this->getValueByFieldset("valigara_password", "authentication"),
                "action" => "magento_save_soap_creds",
                "soap_username" => $this->getFieldsetDataValue("soap_username"),
                "soap_apikey" => $this->getFieldsetDataValue("soap_apikey"),
                "soap_url" => $this->getFieldsetDataValue("soap_apiurl"),
            );
        
        if($data["soap_username"]=="" || $data["soap_apikey"]=="" || $data["soap_url"]=="") {
            $GLOBALS['Valigara_SOAP_Connected'] = 0;
            Mage::getSingleton('core/session')->addWarning("Please fill out the SOAP/XML connection setup fields");
            return false;
        } else {
            $result = $this->valigara_curl($data);
            if(@$result["status"] == '100') {
                $GLOBALS['Valigara_SOAP_Connected'] = 1;
                Mage::getSingleton('core/session')->addSuccess($result["data"]);
                return true;
            } else {
                $GLOBALS['Valigara_SOAP_Connected'] = 0;
                Mage::getSingleton('core/session')->addError($result["data"]);
            }
        }
        return false;
    }
    
    private function valigara_curl($data) {
        $ch = curl_init($this->api_url);
        $encoded = '';
        foreach ($data as $name => $value) {
            $encoded .= urlencode($name) . '=' . urlencode($value) . '&';
        }
        curl_setopt($ch, CURLOPT_POSTFIELDS, $encoded);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, true);
        $output = curl_exec($ch);

        curl_close($ch);
        $result = json_decode($output, true);
        if(json_last_error() === JSON_ERROR_NONE) {
            return $result;
        } else {
            return false;
        }
    }
}
?>
