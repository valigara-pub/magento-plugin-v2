<?php

/**
 * @author antons <antons@valigar.com>
 */
class Valigara_ConnectAPI_Model_Custom_Api extends Mage_Api_Model_Resource_Abstract {

    /**
     * get attribute set groups
     * @access public
     * @param mixed $filters
     * @return array
     */
    public function items($filters = null) {
        $result = array();
        $attributeSetCollection = Mage::getResourceModel('eav/entity_attribute_group_collection')->load();

        $apiHelper = Mage::helper('api');
        $filters = $apiHelper->parseFilters($filters);
        foreach ($attributeSetCollection as $id => $attributeGroup) {
            $result[] = array(
                'group_id'=>$attributeGroup->getAttributeGroupId(),
                'name'=>$attributeGroup->getAttributeGroupName(),
                'set_id'=>$attributeGroup->getAttributeSetId(),
            );
        }
        return $result;
    }

}
